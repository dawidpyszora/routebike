package com.example.dawid.routebike;

/**
 * Created by Dawid on 2017-02-16.
 */

public class UserLocation {

    private long created;
    private double latitude;
    private double longitiude;
    private double hight;
    private double speed;
    private double distance;

    public long getCreated() {
        return created;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitiude() {
        return longitiude;
    }

    public double getHight() {
        return hight;
    }

    public double getSpeed() {
        return speed;
    }

    public double getDistance() {
        return distance;
    }
}
